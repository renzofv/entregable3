const express = require('express');
const body_parser = require('body-parser');
const app = express();
const port = 8005;
const URL_BASE = '/apitechu/v1'
const users = require('./user.json');

app.listen(port, function(){
    console.log('NodeJS listening in port: ' + port);
});

app.use(body_parser.json())


app.get('/holamundo',
    function(request, response){
        response.send('Hola Mundo');
    }
)

app.get(URL_BASE+'/users',
    function(request, response){
        response.send(users);
    }
)

app.get(URL_BASE+'/users/:id',
    function(request, response){
        let pos = request.params.id - 1;
        response.send(users[pos]);
    }
)

app.post(URL_BASE+'/users',
    function(request, response){
        let pos = users.length + 1;
        let new_user = {
            "id": pos,
            "nombres": request.body.first_name,
            "apellidos": request.body.last_name,
            "username": request.body.email,
            "password": request.body.password
        }
        users.push(new_user);
        response.status(201).send(new_user);
    }
)

app.put(URL_BASE+'/users/:id',
    function(request, response){
        let pos = request.params.id - 1;
        let put_user = users[pos];
        put_user.nombres = request.body.first_name;
        put_user.apellidos = request.body.last_name;
        put_user.username = request.body.email;
        put_user.password = request.body.password;
        users[pos] = put_user;
        response.send(put_user);
    }
)

app.delete(URL_BASE+'/users/:id',
    function(request, response){
        let pos = users.findIndex(user => user.id == request.params.id);
        users.splice(pos, 1);
        response.send({"msg": "Usuario eliminado: "+ request.params.id + ", en posicion: "+ pos});
    }
)

app.get(URL_BASE + '/users',
  function(req, res) {
    let pos = users.findIndex(user => user.id == req.query.id);
    res.send(users[pos - 1]);
    respuesta.send({"msg" : "GET con query string"});
});

app.post(URL_BASE + '/login',
  function(request, response) {
    var user = request.body.email;
    var pass = request.body.password;
    for(us of users) {
      if(us.email == user) {
        if(us.password == pass) {
          us.logged = true;
          writeUserDataToFile(users);
          response.send({"msg" : "Login correcto.", "idUsuario" : us.id, "logged" : "true"});
        } else {
          response.status(400).send({"msg" : "Login incorrecto."});
        }
      }
    }
});

function writeUserDataToFile(data) {
    var fs = require('fs');
    var jsonUserData = JSON.stringify(data);
    fs.writeFile("./user.json", jsonUserData, "utf8",
     function(err) {
       if(err) {
         console.log(err);
       } else {
         console.log("Datos escritos en 'users.json'.");
       }
     }); }

app.post(URL_BASE + '/logout/:id',
  function(request, response) {
    var userId = request.params.id;
    for(us of users) {
      if(us.id == userId) {
        if(us.logged) {
          delete us.logged;
          writeUserDataToFile(users);
          response.send({"msg" : "Logout correcto.", "idUsuario" : us.id});
        } else {
          response.status(400).send({"msg" : "Logout incorrecto."});
        }
      }
    }
});


app.get(URL_BASE+'/total_users',
    function(request, response){
        response.send({"num_usuarios":users.length});
    }
)