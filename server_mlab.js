require('dotenv').config();
const express = require('express');
const body_parser = require('body-parser');
const request_json = require('request-json');
const app = express();
const port = 8005;
const URL_BASE = '/apitechu/v2';
const users = require('./user.json');
const URL_DATABASE = 'https://api.mlab.com/api/1/databases/techu28db/collections/';
const apikey_mlab ='apiKey=NQCR6_EMDAdqyM6VEWg3scF_k32uwvHF';
const field_param = 'f={"_id":0}';

app.listen(port, function(){
    console.log('NodeJS listening in port: ' + port);
});

app.use(body_parser.json())


app.get('/holamundo',
    function(request, response){
        response.send('Hola Mundo');
    }
)

app.get(URL_BASE+'/users',
    function(request, response){
        const http_client = request_json.createClient(URL_DATABASE);
        http_client.get(`usuario?${field_param}&${apikey_mlab}`, 
          function(error, res_mlab, body){
            var msg = {};
            if(error) {
              msg = {"msg" : "Error al recuperar usuarios de mLab."}
                response.status(500);
            } else {
              if(body.length > 0) {
                msg = body;
              } else {
                msg = {"msg" : "Usuario no encontrado."};
                response.status(404);
              }
            }
            response.send(msg);
          });
    }
)

app.get(URL_BASE+'/users/:id',
    function(request, response){          
      const http_client = request_json.createClient(URL_DATABASE);
      let query_param = `q={"id_user":${request.params.id}}`;
      let without_account_field_param = 'f={"_id":0, "account": 0}';
      http_client.get(`usuario?${query_param}&${without_account_field_param}&${apikey_mlab}`, 
        function(error, res_mlab, body){
          var msg = {};
          if(error) {
            msg = {"msg" : "Error al recuperar usuario de mLab"}
              response.status(500);
          } else {
            if(body.length > 0) {
              msg = body[0];
            } else {
              response.status(404);
            }
          }
          response.send(msg);
        });
    }
)

app.get(URL_BASE+'/users/:id/accounts',
    function(request, response){          
      const http_client = request_json.createClient(URL_DATABASE);
      let query_param = `q={"id_user":${request.params.id}}`;
      let account_field_param = 'f={"_id":0, "account": 1}';
      http_client.get(`usuario?${query_param}&${account_field_param}&${apikey_mlab}`, 
        function(error, res_mlab, body){
          var msg = {};
          if(error) {
            msg = {"msg" : "Error al recuperar usuario de mLab"}
              response.status(500);
          } else {
            if(body.length > 0) {
              msg = body[0].account;
            } else {
              msg = {"msg" : `Usuario no encontrado ${request.params.id}`};
              response.status(404);
            }
          }
          response.send(msg);
        });
    }
)

app.post(URL_BASE+'/users',
    function(request, response){
      const http_client = request_json.createClient(URL_DATABASE);
      let count_param = 'c=true';
      http_client.get(`usuario?${count_param}&${apikey_mlab}`, 
        function(error, res_mlab, count){            
          let newId = count + 1;
          let newUser = {
              "id": newId,
              "nombres": request.body.first_name,
              "apellidos": request.body.last_name,
              "username": request.body.email,
              "password": request.body.password
          };
          http_client.post(`usuario?&${apikey_mlab}`, newUser, 
            function(error, res_mlab, body){
              response.status(201).send(body);
            });
        });
    }
)

app.put(URL_BASE+'/users/:id',
    function(request, response){      
      const http_client = request_json.createClient(URL_DATABASE);
      let query_param = `q={"id_user":${request.params.id}}`;
      let field_param = 'f={"_id":1}';
      http_client.get(`usuario?${field_param}&${query_param}&${apikey_mlab}`, 
        function(error, res_mlab, body){
          let userId = body[0]._id.$oid;       
          var updateUserComand = {
            "$set": request.body
          };
          http_client.put(`usuario/${userId}?&${apikey_mlab}`, updateUserComand,
            function(error, res_mlab, body){
              response.status(200).send(body);
            });
        });
    }
)

app.delete(URL_BASE+'/users/:id',
    function(request, response){
      const http_client = request_json.createClient(URL_DATABASE);
      let query_param = `q={"id_user":${request.params.id}}`;
      let field_param = 'f={"_id":1}';
      http_client.get(`usuario?${field_param}&${query_param}&${apikey_mlab}`, 
        function(error, res_mlab, body){
          let userId = body[0]._id.$oid;
          http_client.delete(`usuario/${userId}?&${apikey_mlab}`, 
          function(error, res_mlab, body){
            response.status(200).send(body);
          });
        });
    }
)

// LOGIN - users.json
app.post(URL_BASE + '/login',
  function(request, response) {
    const http_client = request_json.createClient(URL_DATABASE);
    var pass = request.body.password;
    let query_param = `q={"email":"${user}","password":"${pass}"}`;
    let field_param = 'f={"_id":1,"id":1,"nombres":1}';
    let limit_param = 'l=1';
    http_client.get(`usuario?${limit_param}&${field_param}&${query_param}&${apikey_mlab}`, 
      function(error, res_mlab, body){
        if(error){
          return response.status(500).send({"msg": "Error en petición a mLab."});
        }
        if (body.length < 1) {
          return response.status(404).send({"msg":"Usuario no válido."});
        }
        let userFound = body[0];
        let userId = userFound._id.$oid;     
        var loginComand = {
          "$set": {"logged": true}
        };
        http_client.put(`usuario/${userId}?&${apikey_mlab}`, loginComand,
          function(error, res_mlab, body){
            response.send({'msg':'Login exitoso', 'username':user, 'id':userFound.id_user, 'nombres':userFound.first_name});
          }
        );
      }
    );
});

// LOGOUT - users.json
app.post(URL_BASE + '/logout',
  function(request, response) {
    const http_client = request_json.createClient(URL_DATABASE);
    let user = request.body.email;
    let query_param = `q={"email":"${user}","logged":true}`;
    let field_param = 'f={"_id":1}';
    let limit_param = 'l=1';
    
    http_client.get(`usuario?${limit_param}&${field_param}&${query_param}&${apikey_mlab}`, 
      function(error, res_mlab, body){
        if(error){
          return response.status(500).send({"msg": "Error en petición a mLab."});
        }
        if (body.length < 1) {
          return response.status(404).send({"msg":"Logout failed!"});
        } 
        let userFound = body[0];
        let userId = userFound._id.$oid;      
        var logoutComand = {
          "$unset": {"logged": true}
        };
        http_client.put(`usuario/${userId}?&${apikey_mlab}`, logoutComand,
          function(error, res_mlab, body){
            response.send({'msg':'Logout correcto', 'email':user});
          }
        );
      }
    );
});


app.get(URL_BASE+'/total_users',
    function(request, response){
             
      const http_client = request_json.createClient(URL_DATABASE);
      let count_param = 'c=true';
      http_client.get(`usuario?${count_param}&${apikey_mlab}`, 
        function(error, res_mlab, body){
          var msg = {};
          if(error) {
            msg = {"msg" : "Error al recuperar usuario de mLab"}
              response.status(500);
          } else {
            msg = {"num_usuarios: ":body};
          }
          response.send(msg);
        });
    }
)